from django.urls import path, include
from todos.views import todo_list
from todos.views import todo_item, create_TodoList, edit_list


urlpatterns = [
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("create/", create_TodoList, name="todo_list_create"),
    path("<int:id>/", todo_item, name="todo_list_detail"),
    path("", todo_list, name="todo_list_list"),
]
