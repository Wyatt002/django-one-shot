from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.models import TodoItem
from todos.forms import TodoListForm



def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def todo_item(request, id):
    item = TodoList.objects.get(id=id)
    context = {
        "todo_item": item,
    }
    return render(request, "todos/detail.html", context)

def create_TodoList(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
        TodoList = form.save()
        return redirect("todo_list_detail", id=TodoList.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoListForm()

  context = {
    "form": form
  }

  return render(request, "todos/create.html", context)


def edit_list(request, id):
  post = get_object_or_404(TodoList, id=id)
  if request.method == "POST":
    form = TodoListForm(request.POST, instance=post)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      model_instance = form.save()
      return redirect("todo_list_detail", id=TodoList.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoListForm(instance=post)

  context = {
    "form": form
  }

  return render(request, "todos/edit.html", context)
